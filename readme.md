# Google Drive Backup

Skrypty napisane do wykonywania automatycznego backup'u (np. VPS'a) - do poprawnego działania potrzebują kluczy wygenerowanych w Google API (client_secrets.json, credentials.json, settings.yaml) oraz.. wolnej przstrzeni na dysku Google.

* backupGDrive_v2 - skrypt do wykonywania backup'ów
* GDriveControl.py - zarządzanie Dyskiem Google
