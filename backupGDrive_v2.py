import GDriveControl
import os
import time


gDrive = GDriveControl.GDriveControl()

dataAktualna = time.strftime("%Y%m%d", time.localtime())
backupFileName = str(dataAktualna)+"_UbuntuBackup.tar.gz"
backupFolderName = "MYUbuntuBackup"
uniqueIDFolder = None
maxNumFilesBackup = 2
backupScriptDir = "/home/lordmaksym/backupGDrive" #dir to exclude

print "Tworze Backup"
#tworzenie backupu
os.system("tar -cvpzf "+backupFileName+" --exclude="+backupScriptDir+" --one-file-system /")

print "Utworzono"

print "Sprawdzanie czy istnieje folder"
#sprawdzanie czy istnieje folder
if gDrive.folderExist(backupFolderName) == False:
    print "Nie istnieje - tworzenie"
    # jezeli nie - tworzymy go
    gDrive.createFolder(backupFolderName)

# pobieranie unikalnego id folderu
listFolders = gDrive.getListFolder()

for folder in listFolders:
    if folder['title'] == backupFolderName:
        uniqueIDFolder = folder['id']
        break

# pobieranie listy plikow w folderze
filesInFolder = gDrive.getListFiles(uniqueIDFolder)

# jezeli za duzo plikow - treba usunac najstarszy
if len(filesInFolder) >= maxNumFilesBackup:
    print "Za duzo plikow - usuwanie najstarszego"
    oldestFile = None

    # wybor najstarszego pliku
    for file in filesInFolder:

        # print file['title'] + " " + file['id'] + " " + file['createdDate']

        if oldestFile == None:
            oldestFile = file
        else:
            if oldestFile['createdDate'] > file['createdDate']:
                oldestFile = file

    # usuniecie
    oldestFile.Delete()

print "Upload"
# upload
gDrive.uploadFile(backupFileName,uniqueIDFolder)

print "Done"


