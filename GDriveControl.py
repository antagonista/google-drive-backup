from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive


class GDriveControl():

    gauth = None
    drive = None

    def __init__(self):
        self.auth()

    def auth(self):
        self.gauth = GoogleAuth()
        self.gauth.LocalWebserverAuth()
        self.drive = GoogleDrive(self.gauth)

    def uploadFile(self,fileName,idFolder = "root"):

        try:
            file = self.drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": idFolder}]})
            file.SetContentFile(fileName)
            file.Upload()
            return True

        except:
            return False

    def deleteItem(self,fileName,uniqueID = None,idFolder = "root",trash = False):
        # if trash = False -> Permanently delete the file.
        # if trash = True -> Move file to trash.

        done = False

        listFiles = self.getFullList(idFolder)

        for file in listFiles:

            if file['title'] == fileName:

                if uniqueID != None:

                    if file['id'] == uniqueID:

                        if trash == True:
                            file.Trash()
                        else:
                            file.Delete()

                        done = True

                        break

                else:

                    if trash == True:
                        file.Trash()
                    else:
                        file.Delete()

                    done = True

        return done

    def folderExist(self,nameFolder,uniqueIdFolder = None,idFolderToCheck = "root"):

        found = False

        listFolders = self.getListFolder(idFolderToCheck)

        for folder in listFolders:
            if folder['title'] == nameFolder:

                if uniqueIdFolder != None:

                    if folder['id'] == uniqueIdFolder:

                        found = True
                        break

                else:
                    found = True
                    break

        return found

    def fileExist(self,nameFile,uniqueID = None,idFolderToCheck = "root"):

        found = False

        listFiles = self.getListFiles(idFolderToCheck)

        for file in listFiles:
            if file['title'] == nameFile:

                if uniqueID != None:

                    if file['id'] == uniqueID:
                        found = True
                        break

                else:
                    found = True
                    break

        return found

    def getFullList(self,idFolder = "root"):

        metadata = {'q': "'" + idFolder + "' in parents and trashed=false"}

        file_list = self.drive.ListFile(metadata).GetList()
        return file_list

    def getListFolder(self,idFolder = "root"):

        metadata = {"q": "'" + idFolder + "' in parents and mimeType='application/vnd.google-apps.folder' and trashed=false"}

        listFolder = self.drive.ListFile(metadata).GetList()
        return listFolder

    def getListFiles(self,idFolder = "root"):
        allFiles = self.getFullList(idFolder)

        fileList = []

        for file in allFiles:
            if file['mimeType'] != "application/vnd.google-apps.folder":
                fileList.append(file)

        return fileList

    def createFolder(self,nameFolder,idFolder = None):
        try:

            folder_metadata = {'title': nameFolder, 'mimeType': 'application/vnd.google-apps.folder'}

            if idFolder != None:
                folder_metadata['parents'] = [{"kind": "drive#fileLink", "id": idFolder}]

            folder = self.drive.CreateFile(folder_metadata)
            folder.Upload()
            return True

        except:
            return False





